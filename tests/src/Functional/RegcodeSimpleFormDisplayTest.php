<?php

namespace Drupal\Tests\regcode_simple\Functional;

use Drupal\Core\Entity\Entity\EntityFormDisplay;
use Drupal\Tests\BrowserTestBase;

/**
 * Tests user form display.
 *
 * @group regcode_simple
 */
class RegcodeSimpleFormDisplayTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  public static $modules = ['regcode_simple'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    // Configure the module.
    \Drupal::configFactory()->getEditable('regcode_simple.settings')
      ->set('type', 'code_plain_text')
      ->set('code', 'foo')
      ->save();

    // Create the default user form display.
    $form_display = EntityFormDisplay::create([
      'targetEntityType' => 'user',
      'bundle' => 'user',
      'mode' => 'default',
      'status' => TRUE,
    ]);
    $form_display->save();
  }

  /**
   * Tests that the registration code field is displayed on the register form.
   */
  public function testFormDisplay() {
    $this->drupalGet('user/register');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('Registration code');
  }

  /**
   * Tests that the registration code field is *not* displayed when hidden.
   *
   * This can be useful when this module is used in combination with multiple
   * registration forms.
   */
  public function testHiddenRegistrationCodeField() {
    // Don't show registration code field on register form.
    $this->container->get('entity_type.manager')
      ->getStorage('entity_form_display')
      ->load('user.user.default')
      ->removeComponent('regcode_simple')
      ->save();

    $this->drupalGet('user/register');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->fieldNotExists('regcode_simple');
  }

}
