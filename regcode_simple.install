<?php

/**
 * @file
 * Install, update and uninstall functions for the regcode_simple module.
 */

use Drupal\Core\Url;

/**
 * Implements hook_uninstall().
 */
function regcode_simple_uninstall() {
  \Drupal::service('config.factory')
    ->getEditable('regcode_simple.settings')
    ->delete();
}

/**
 * Implements hook_requirements().
 *
 * Display information if registration code is or is not not set.
 */
function regcode_simple_requirements($phase) {
  $requirements = [];

  // Check the server's ability to indicate upload progress.
  if ($phase == 'runtime') {
    $config = \Drupal::config('regcode_simple.settings');
    if ($config->get('type')) {
      $severity = NULL;
      $value = t('Enabled.');
    }
    else {
      $severity = REQUIREMENT_WARNING;
      $value = t('Code is not set.');
    }
    $path = Url::fromRoute('entity.user.admin_form', [], ['fragment' => 'edit-regcode-type--wrapper'])
      ->toString();
    $description = t('<a href="@conf_path">Set or change code here</a>.', ['@conf_path' => $path]);
    $requirements['regcode_simple'] = [
      'title' => t('Registration Code (Simple)'),
      'value' => $value,
      'severity' => $severity,
      'description' => $description,
    ];
  }

  return $requirements;
}
